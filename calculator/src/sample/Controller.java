package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    Label window;

    StringBuilder text = new StringBuilder();

    @FXML
    public void clickedZero() {
        text.append("0");
        window.setText(text.toString());
    }

    @FXML
    public void clickedOne() {
        text.append("1");
        window.setText(text.toString());
    }

    @FXML
    public void clickedTwo() {
        text.append("2");
        window.setText(text.toString());
    }

    @FXML
    public void clickedThree() {
        text.append("3");
        window.setText(text.toString());
    }

    @FXML
    public void clickedFour() {
        text.append("4");
        window.setText(text.toString());
    }

    @FXML
    public void clickedFive() {
        text.append("5");
        window.setText(text.toString());
    }

    @FXML
    public void clickedSix() {
        text.append("6");
        window.setText(text.toString());
    }

    @FXML
    public void clickedSeven() {
        text.append("7");
        window.setText(text.toString());
    }

    @FXML
    public void clickedEight() {
        text.append("8");
        window.setText(text.toString());
    }

    @FXML
    public void clickedNine() {
        text.append("9");
        window.setText(text.toString());
    }

    @FXML
    public void clickedDivision() {
        deleteArithmeticOperation();
        text.append(" ÷ ");
        window.setText(text.toString());
    }

    @FXML
    public void clickedMultiplication() {
        deleteArithmeticOperation();
        text.append(" x ");
        window.setText(text.toString());
    }

    @FXML
    public void clickedSubtraction() {
        deleteArithmeticOperation();
        text.append(" - ");
        window.setText(text.toString());
    }

    @FXML
    public void clickedAdd() {
        deleteArithmeticOperation();
        text.append(" + ");
        window.setText(text.toString());
    }

    @FXML
    public void clickedComma() {
        text.append(",");
        window.setText(text.toString());
    }

    @FXML
    public void clickedNegation() {
        deleteArithmeticOperation();
        text.append(" * -1");
        window.setText(text.toString());
    }

    @FXML
    public void clickedClear() {
        text.setLength(0);
        window.setText("");
    }

    @FXML
    public void deleteChar() {
        if (deleteArithmeticOperation() == false && text.length() > 0) {
            text.deleteCharAt(text.length() - 1);
        }
        window.setText(text.toString());
    }

    @FXML
    public void clickedResult() {
        Double value = eval(text.toString().replace("x", "*").replace("÷", "/").replace(",", "."));
        window.setText(value.toString().replace(".", ","));
    }

    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
                return x;
            }

            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (; ; ) {
                    if (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus
                double x;
                int startPos = this.pos;
                if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else {

                    throw new RuntimeException("Unexpected: " + (char) ch);
                }
                return x;
            }
        }.parse();
    }

    private boolean deleteArithmeticOperation() {
        if (text.length() > 0 && text.charAt(text.length() - 1) == ' ') {
            for (int i = 0; i < 3; ++i) {
                text.deleteCharAt(text.length() - 1);
            }
            return true;
        }
        return false;
    }
}
